(function($) {
    $(document).ready(function() {
		
        $("#vcode").on('click', function(e) {
            $.ajax({
                url: Drupal.settings.basePath + 'myaccount/register/pin_resend',
                type: "POST",
                success: function(data) {
                    window.location = data;
                }
            });
        });

        //BEGIN: Register Step 1 - form validation
		$("#myaccount-registration-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$("#replace_validation_div").html('');
		});
        //BEGIN: Register Step 2 - form validation
		$("#myaccount-reg-verification-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$("#replace_validation_div").html('');
		});        
		//BEGIN: Register Step 3 - form validation
		$("#myaccount-reg-continue-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$("#replace_validation_div").html('');
		});		
		//Login Form Validation
		$("#myaccount-login-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$("#replace_validation_div").html('');
		});
		
        // Forgot password form validation
		$("#myaccount-forgot-password-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$("#replace_validation_div").html('');
			$(".error").remove();
		});        
		
		// Reset password form validation
		$("#myaccount-reset-password-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$("#replace_validation_div").html('');
		});		

    });
 
})(jQuery);