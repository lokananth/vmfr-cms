<?php
/**
 * Country List 
 * @return type
 */
function get_country_list() {
	global $config;
	$countryOptions = array();
	$param = array(
		"sitecode" => $config['sitecode'],
		"brand" => $config['brand']
	);	
	$response = ApiPostNew(LLOM_COUNTRY_LIST, $param);
	//echo "<pre>" . API_HOST . LLOM_COUNTRY_LIST . '<br>' ; print_r($param); print_r($response); exit;
	$countryOptions[] = 'Select Country';
	foreach($response as $country){
		$countryOptions[$country['country']] = ucwords(strtolower($country['country']));
	}
	return $countryOptions;
}
/**
 * Plan List 
 * @return type
 */
function get_llom_plan_list($selected_country) {
	global $config;
	$planOptions = array();
	if(!empty($selected_country)){
		$param = array(
			"name" => $selected_country,
			"sitecode" => $config['sitecode'],
			"brand" => $config['brand']
		);	
		$response = ApiPostNew(LLOM_PLAN_RATE_LIST, $param);
		//echo "<pre>" . API_HOST . LLOM_PLAN_RATE_LIST . '<br>' ; print_r($param); print_r($response); exit;
		foreach($response as $plan){
			$key = $plan['svc_id'].'~'.$plan['Amount'];
			$planOptions[base64_encode($key)] = $plan['currency'] . ' ' . $plan['Amount'] . ' ' . $plan['desc'];
		}
	}
	$_SESSION['plan_list'] = $planOptions;
	return $planOptions;
}


/**
 * Get Area List
 */
function get_area_list() {
	global $config;
	$cityOptions = array();
	if(!empty($_SESSION['llom_step1']['Country'])){
		$selected_country = $_SESSION['llom_step1']['Country'];
		$param = array(
			"country_name" => $selected_country,
			"sitecode" => $config['sitecode'],
			"brand" => $config['brand']
		);	
		$response = ApiPostNew(LLOM_AREA_LIST, $param);
		//echo "<pre>" . API_HOST . LLOM_AREA_LIST . '<br>' ; print_r($param); print_r($response); exit;
		$cityOptions[] = 'Select Area';
		foreach($response as $key){
			if(isset($key) && ($key == -1 || $key == 'fail')){ break; }
			$cityOptions[$key['area_name']] = $key['area_name'];
		}
	}
	return $cityOptions;
}


/**
 * Get Area Code List
*/
function get_area_code_list($selected_city) {
	global $config;
	$areaCodeOptions[] = 'Select Area Code';
	if(!empty($selected_city)){
		$selected_country = $_SESSION['llom_step1']['Country'];
		$param = array(
			"country_name" => $selected_country,
			"area_name" => $selected_city,
			"sitecode" => $config['sitecode'],
			"brand" => $config['brand']
		);	
		$response = ApiPostNew(LLOM_AREA_CODE_LIST, $param);
		//echo "<pre>" . API_HOST . LLOM_AREA_CODE_LIST . '<br>' ; print_r($param); print_r($response); exit;	
		foreach($response as $key){
			if(isset($key) && ($key == -1 || $key == 'fail')){ break; }
			$areaCodeOptions[$key['area_code']] = $key['area_code'];
		}
	}
	return $areaCodeOptions;
}



/**
 * Get Landline Number List
*/
function get_landline_number_list($selected_city, $selected_area_code) {
	global $config;
	$numberOptions[] = 'Select Number';
	$numberList = array();
	if(!empty($selected_city) && !empty($selected_area_code)){
		$data = array(
			'country_name' => $_SESSION['llom_step1']['Country'],
			'area_name' => $selected_city,
			'area_code' => $selected_area_code,
			'sitecode' => $config['sitecode'],
			'brand' => $config['brand']
		);
		$response = ApiPostNew(LLOM_NUMBER_LIST, $data);
		//echo '<pre> URL : '.LLOM_NUMBER_LIST.'<br>'; print_r($data); print_r($response); echo '</pre>';
		foreach($response as $key){
			if(isset($key) && ($key == -1 || $key == 'fail')){ break; }
			$numberOptions[$key['normalized_number']] = $key['normalized_number'];
			$numberList[] = $key['normalized_number'];
		}
	}
	$_SESSION['llom_numbers_list'] = $numberList;
	return $numberOptions;
}
    
/**
 * Verify Security Code
 */
function verify_security_code($PinCode) {
	$status = '';
	if (!empty($PinCode)):
		if ($_SESSION['OTP'] == $PinCode) {
			$status = '0';
		}else{
			$status = '-1';
		}
	endif;
	return $status;
}

function release_llom_numbers(){
	global $config;
	$param = array(
		'sitecode' => $config['sitecode'],
		'brand' => $config['brand'],
		'number1' => $_SESSION['llom_numbers_list'][0],
		'number2' => $_SESSION['llom_numbers_list'][1],
		'number3' => $_SESSION['llom_numbers_list'][2],
		'number4' => $_SESSION['llom_numbers_list'][3],
		'number5' => $_SESSION['llom_numbers_list'][4]
	);	
	$rel_response = apiPostNew(LLOM_RELEASE_NUMBER, $param);
	if($config['debug'] == 1){
		mail('m.manokaran@mundio.com','LLOM_RELEASE_NUMBER '.date("Y-m-d H:i:s"), API_HOST . LLOM_RELEASE_NUMBER . print_r($param, true) . print_r($rel_response, true) );
	}	
}
function subscribe_llom(){
	global $config;
	$data = array(
		'sitecode' => $config['sitecode'],
		'brand' => $config['brand'],
		'acctype' => 1,
		'accinfo' => $_SESSION['llom_step2']['Mobile_Number'],
		'svc_id' => $_SESSION['llom_step1']['Plan'],
		'input_regnumber' => $_SESSION['llom_step2']['LLOM_Number'],
		'called_by' => $config['sourcereg'],
		'paymode' => $_SESSION['llom_step2']['Pay_Option'],
		'processurl' => $_SESSION['ref_url']
	);	
	$response = apiPostNew(LLOM_SUBSCRIBE, $data);	
	if($config['debug'] == 1){
		mail('m.manokaran@mundio.com','LLOM_SUBSCRIBE '.date("Y-m-d H:i:s"), API_HOST . LLOM_SUBSCRIBE . print_r($data, true) . print_r($response, true) . $_SERVER['HTTP_REFERER'] );
	}	
	return $response;
}

