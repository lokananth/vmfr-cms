(function ($) {
$(document).ready(function () {
    $('.panel').matchHeight();
	$('.panel-heading').matchHeight();
	$('.top-part').matchHeight();
	$('.bottom-part').matchHeight();
	
	$("#Pause").click(function () { 
		   $('#slider').data('nivoslider').stop(); 
	});        
	$("#Play").click(function () { 
	   $('#slider').data('nivoslider').start();
	});  
	
	
	$('.form-submit').on('click', function(){
		if($('.required').val()==''){
			$('.required').addClass('error').parent().addClass('has-error error-processed');
		}		
	});
	
	$('.required').on('keyup change', function(){
		if($(this).val()!=''){
			$(this).removeClass('error').parent().removeClass('has-error error-processed');
			$(".alert").fadeOut();
		}		
	});

		
	$("#edit-submitted-email,#edit-submitted-e-post").attr("maxlength",60);  
	$("#edit-submitted-telefoon-nummer,#edit-submitted-contact-number").attr("maxlength",15);

	$('input.disablecopypaste').bind('copy paste', function (e) {
		   e.preventDefault();
	});

	$(".disablecopypaste,.disable_mnumber").keypress(function (e) {
		//if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			//display error message
			$("#errmsg").html("Digits Only").show().fadeOut("slow");
			return false;
		}
	});	  
	$('.form-submit').on('click', function(){
	   $(this).closest('.panel').css('height','auto');
	})
	
	$('#edit-login-btn-submit').click( function (){
		if ($('#edit-condtion').is(':checked')) {
			var username = $('#username').val();
			var password = $('#edit-password').val();
			// set cookies to expire in 14 days
			$.cookie('username', username, { expires: 14 });
			$.cookie('password', password, { expires: 14 });
			$.cookie('remember', true, { expires: 14 });
		} else {
			// reset cookies
			$.cookie('username', null);
			$.cookie('password', null);
			$.cookie('remember', null);
		}
	});

	var remember = $.cookie('remember');
	if ( remember == 'true' ) {
		var username = $.cookie('username');
		var password = $.cookie('password');
		// autofill the fields
		$('#edit-condtion').attr("checked" , true );
		$('#username').val(username);
		$('#edit-password').val(password);
	}
	
	$.fn.restrictInputs = function(restrictPattern){
		var targets = $(this);
		var pattern = restrictPattern || /[^a-zA-Z0-9@._-]/g // some default pattern

		var restrictHandler = function(){
			var val = $(this).val();
			var newVal = val.replace(pattern, '');

			if (val !== newVal) { // To prevent selection and keyboard navigation issues
				$(this).val(newVal);
			}
		};

		targets.on('keyup', restrictHandler);
		targets.on('paste', restrictHandler);
		targets.on('change', restrictHandler);
	};

	$('#edit-email, #edit-confirm-email, #email').restrictInputs();
 
    $('#edit-password,#edit-confirm-password').keypress(function(key) {
        if(key.charCode == 32) return false;
    });
	
	$('#edit-password,#edit-confirm-password,#edit-confirm-email, #confirm-mobile-number').bind("paste",function(e) {
		e.preventDefault();
	});
 
	$('.visa-c').on('click', function(){
		window.open('http://www.mastercard.co.uk/securecode.html','mywindowtitle','width=700,height=650');
	}); 
	
	$('.master-c').on('click', function(){
		window.open('http://www.visaeurope.com/en/cardholders/verified_by_visa.aspx','mywindowtitle','width=700,height=650');
	});

 	$('#card_number,#security_code').bind("cut copy paste",function(e) {
		e.preventDefault();
	}); 

	$.fn.scrollView = function () {
		return this.each(function () {  
			$('html, body').animate({
				scrollTop: $(this).parent().offset().top
			}, 'slow' );
		});
	}

	if ( $('#main-content').next().is('.alert-block')) {
		$('.alert-block').scrollView();
	} else {
		$('#topup-registration-form, #status-help, #topup-review-confirm, #topup-payment-form, #myaccount-login-form, #myaccount-registration-form, #myaccount-forgot-password-form, #freesim-registration-form, .status-help,.topup_payment, #topup-paysafe-step2-form,.other_payment, #paym-registration-form, #paym-number-validate-form, #paym-personal-details-form, #paym-direct-debit-enrollment-form, #paym-payment-form, #paym-payment-review, #myaccount-reg-verification-form, #myaccount-reg-continue-form, #topup-paysafe-review-confirm, #myaccount-reset-password-form').parent().parent().scrollView();
	}		
	
	//Allow Numbers Only
	$('#security_code, #pin_code, #edit-captcha-response, #edit-contact-number, #edit-submitted-contact-number, #edit-submitted-telefoon-nummer, #edit-issue-no, #edit-verification-code, #username, #edit-verification-code, #edit-contact-number, #mobile-number, #card_number, #edit-pin-code, #contact_number, #edit-mobile-number, #confirm-mobile-number, .houseno, .bill_houseno').bind('keyup blur', function(e){ 
		if (this.value.match(/[^0-9]/g)) {
			this.value = this.value.replace(/[^0-9]/g, '');
        }		
	});
	
	//Allow Alphabets only
	$("#cardholder_name, #name, #town, #edit-first-name, #edit-last-name, #edit-name, #edit-submitted-first-name, #edit-submitted-last-name, #edit-town").bind('keyup blur', function(e){		
		/*var node = $(this);
		node.next('.errormsg').empty();
		if (String.fromCharCode(e.which).match(/[^a-z A-Z]+/)) {
			return false;
		}*/
		if (this.value.match(/[^a-z A-Z]+/g)) {
			this.value = this.value.replace(/[^a-z A-Z]+/g, '');
        }			
	});

	//Allow Alphanumeric with comma, fullstop & space
	$('#address1, #address2').bind('keyup blur', function(e){ 
		if (this.value.match(/[^a-z A-Z0-9,.]+/g)) {
			this.value = this.value.replace(/[^a-z A-Z0-9,.]+/g, '');
        }		
	});	

	//Load External Loader on Custom Ajax calls & System Ajax Callbacks.
	$(document).bind("ajaxSend", function(){
		$(".loader-bg").show();
	}).bind("ajaxComplete", function(){
		$(".loader-bg").hide();
	});
	
});
})(jQuery);


